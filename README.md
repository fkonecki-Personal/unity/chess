# Chess

Hotseat chess game for two players


## GoogleDrive folder:

- Chess.zip containing a .zip of the "Assets", "ProjectSettings" and “Packages” Unity project folders 

https://drive.google.com/drive/folders/1RzNkDaOADqh-muXPHWC8lzLCtkL2wkVC?usp=sharing


## Unimplemented Features

Checking for chess and pat 
Logic exists in BoardSO in private helper methods part

Variable replay timestep
Needs UI InputField when replay is started, where player can enter wanted timestep. 
Replay class has ReplayCoroutine(int waitTimeSec = 1) that just needs the parameter passed to it.

Replaying promotions
Only moves can be replayed.
Implementation idea:
ReplayData holds lists for promotions (same as for moves).
When promotion happens it is written in those fields.
Promotions would be marked in moves list with -1 for figureId and fieldId corresponding to field of figure being promoted.
When move with figureId of -1 is encountered when replaying data, promotion is executed from promotion lists on coresponding promotionsIndex (like moves with movesIndex)


## Details

### Controlls:

- Click on figure
Selects the figure. When player is on the move, colliders of other players figures are diabled so that they can't be selected and fields under them can be selected

- Click on field
After the figure has been selected and (available) field is selected, figure will be moved.

- Replay
A 
Starts automatic replay from the start of the game, with provided timesteps between moves

M 
Starts manually controlled game replay where Left Arrow takes the replay one move back and Right Arrow takes the replay one move forward


### Scripts:

- FigureType, FigureColor
Enumerations

- PlayerInput
Listens to players mouse inputs for selecting and moving figures and key inputs (A, M, LeftArrow, RightArrow)

- Figure
This script is used as Base class for all figure scripts and it holds basic info about the figure, accessable moves and actions for when figure is selected and moved.

- Field
Holds id of the field used to determine which field user has clicked on.

- FigureDirectional/FigureExact
These scripts are Derived classes of Figure class. 

FigureExact is only used to add distinction between two different types of figure moves. Every class that is derived from this class implements their own specific moves from given position.

FigureDirectional has function for calculating moves of figures that can move unlimited number of steps in certain direction. 
Straight (vertical-horizontal cross) represents the movement of rook, diagonal (diagonal cross) represents the movement of bishop and combination of both the movement of queen.

- Pawn, Rook, Knight, Bishop, Queen, King
These scripts are Derived classes of FigureDirectional/FigureExact classes. Scripts override function for getting available moves for specific figure type and its current position.

- BoardSO
ScriptableObject holding data that reflects the current state of the board and provides functions for manipulating it.

BoardMatrix holds 2D array (8x8) holding ids of figures on fields (-1 field empty). 
FiguresInfo holds data for each figure where index in list coresponds to the id of figure.
AvailableFields contains ids of fields that currently selected figure can move to.

EnPassantField holds the id of field that can be used to execute EnPassant. When player moves a pawn for the first time with 2 steps forward, pawn marks filed behind it as EnPassantField. Pawns check diagonal fields for enemies and EnPassantField (in Pawn class), that are added to their available moves. En Passant is removed (set to -1) if other player does not use it right away.

CastlingFields hold 4 fields that can activate castling (2 for each player). Those fields are removed when one of players moves their rook or king. If rook is moved only one corresponding field is removed. If king is moved both fields for castling for that player are removed. Castling fields are added to available moves when player selects the king. If player has the option to activate 

Promoting option is enabled when player has a pawn on furthest field. When pawn is selected dropdown and button is displayed on canvas. Player selects A figure to promote the pawn to and clicks Promote button to activate promotion. When promotion is activated pawn game object replaces its Mesh, Material and Figure derived script with those of target figure.

- Channels and Listeners
Channels and listeners are used to preserve better encapsulation and to provide publisher-observer communication with figures 

EventChannel, EventListener
Base classs for implementing channels and channel listeners.

FigureSelectChannel, FigureMoveChannel
Derived from EventChannel. Implement communication channels with figures for figure select and figure move events.

FigureSelectListener, FigureMoveListener
Derived classes from EventListener. Implements listener script component for listening events on specific channel

Possible improvement: Merge these two channels into one to have a single channel responsible for sending data and commands to figures

- Replay, ReplayData, Move
Replay
Singleton class that provides functions for controlling replay and running automatic replay coroutine

ReplayData
ScriptableObject for storing replay data (moves). 

Move
Class used to represent a single move in replay.

- SafeAreaFilter, UIScript
SafeArea is used to resize attached area for mobile devices that don't have a rectangular screen.

UIScript is used for toggling displayed color of current player and for triggering promoting action.


## Assets used:
https://assetstore.unity.com/packages/3d/props/free-low-poly-chess-set-116856



