using UnityEngine;
using System.Collections.Generic;

namespace Chess
{
    [CreateAssetMenu(menuName = "Chess/Channels/FigureMove")]
    public class FigureMoveChannel : EventChannel
    {
        List<FigureMoveListener> _listeners = new();

        public override void AddListener(EventListener listener)
        {
            _listeners.Add((FigureMoveListener) listener);
        }

        public override void RemoveListener(EventListener listener)
        {
            _listeners.Remove((FigureMoveListener) listener);
        }

        // Figure move event
        public void TriggerEvent(int id, int field)
        {
            for (int i = 0; i < _listeners.Count; ++i)
            {
                _listeners[i].OnEventTriggered(id, field);
            }
        }

        // Position reset event
        public void TriggerEvent()
        {
            for (int i = 0; i < _listeners.Count; ++i)
            {
                _listeners[i].OnEventTriggered();
            }
        }
    }
}
