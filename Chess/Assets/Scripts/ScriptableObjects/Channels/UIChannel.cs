using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chess {
    public class UIChannel : EventChannel
    {
        List<UIListener> _listeners = new();

        public override void AddListener(EventListener listener)
        {
            _listeners.Add((UIListener)listener);
        }

        public override void RemoveListener(EventListener listener)
        {
            _listeners.Remove((UIListener)listener);
        }
        public void TriggerEvent(int figureId)
        {
            for (int i = 0; i < _listeners.Count; ++i)
            {
                _listeners[i].OnEventTriggered(figureId);
            }
        }
    }
}
