using UnityEngine;
using System.Collections.Generic;

namespace Chess
{

    [CreateAssetMenu(menuName = "Chess/Channels/FigureSelect")]
    public class FigureSelectChannel : EventChannel
    {
        List<FigureSelectListener> _listeners = new();

        public override void AddListener(EventListener listener)
        {
            _listeners.Add((FigureSelectListener) listener);
        }

        public override void RemoveListener(EventListener listener)
        {
            _listeners.Remove((FigureSelectListener) listener);
        }
        public void TriggerEvent(int figureId)
        {
            for (int i = 0; i < _listeners.Count; ++i)
            {
                _listeners[i].OnEventTriggered(figureId);
            }
        }
    }
}
