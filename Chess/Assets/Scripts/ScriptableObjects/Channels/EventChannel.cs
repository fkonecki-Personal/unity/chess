using UnityEngine;

namespace Chess
{
    public abstract class EventChannel : ScriptableObject
    {
        public abstract void AddListener(EventListener listener);
        public abstract void RemoveListener(EventListener listener);
    }
}
