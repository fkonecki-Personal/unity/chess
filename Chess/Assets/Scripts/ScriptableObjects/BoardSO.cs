using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    [CreateAssetMenu(fileName = "Board", menuName = "Chess/Board")]
    public class BoardSO : ScriptableObject
    {
        readonly int[,] _initialBoard = {
                { 0,1,2,3,4,5,6,7},
                { 8,9,10,11,12,13,14,15},
                { -1,-1,-1,-1,-1,-1,-1,-1},
                { -1,-1,-1,-1,-1,-1,-1,-1},
                { -1,-1,-1,-1,-1,-1,-1,-1},
                { -1,-1,-1,-1,-1,-1,-1,-1},
                { 16,17,18,19,20,21,22,23},
                { 24,25,26,27,28,29,30,31}
            };

        [SerializeField] FigureSelectChannel _figureSelectChannel;
        [SerializeField] FigureMoveChannel _figureMoveChannel;
        [SerializeField] ReplayData _replayData;
        [SerializeField] GameObject _figures;
        [SerializeField] GameObject _promotionUI;

        public int[,] BoardMatrix { get; set; }
        public FigureColor PlayerTurn { get; set; } = FigureColor.White;
        public List<int> AvailableFields { get; set; } = new();
        public List<Figure> FigureInfos { get; set; } = new(); // index = figureId

        public int EnPassantField { get; set; } = -1;
        public List<int> CastlingFields { get; set; } = new() {2,6,26,30};

        int _selectedFigureId = -1;
        int _selectedFigureField = -1;

//####################-- PRIMARY METHODS --#############################################################

        public void FigureSelected(int figureId) 
        { 
            _selectedFigureId = figureId;
            GetAvailableFieldsFor(figureId);

            int figureRow = PositionOfFigure(figureId) / 8;
            bool promotionActive = IsSelectedFigureOfType(FigureType.Pawn) &&
                (figureRow == 0 || figureRow == 7);

            _promotionUI.SetActive(promotionActive);
        }

        public void FieldSelected(int fieldId)
        {
            if (IsSelectedFigureOfType(FigureType.King) &&
                    CastlingFields.Contains(fieldId) &&
                    IsHorizontalPathToFieldEmptyAndUnexposed(fieldId))
            {
                Castle(fieldId);
                ToggleTurn();
            }
            else
            if (AvailableFields.Contains(fieldId) && !ResultsInCheck(fieldId))
            {
                UpdateMatrix(fieldId);
                UpdateReplayAndFigures(fieldId);

                Figure selectedFigure = FigureInfos[PositionOfFigure(_selectedFigureId)];

                if (IsSelectedFigureOfType(FigureType.Rook) && selectedFigure.NotMoved)
                {
                    if (PlayerTurn.Equals(FigureColor.White))
                        CastlingFields.Remove(_selectedFigureId == 0 ? 2 : 3);
                    else
                        CastlingFields.Remove(_selectedFigureId == 24 ? 26 : 30);
                }

                selectedFigure.NotMoved = false;

                _selectedFigureId = -1;
                _selectedFigureField = -1;

                ToggleTurn();
            }
        }


//####################-- SPECIAL CASE METHODS --########################################################

        void Castle(int fieldId)
        {
            int rookField;
            int rookId;

            if (PlayerTurn.Equals(FigureColor.White))
            {
                bool isLeftCastle = fieldId == 2;
                rookField =  isLeftCastle ? 3 : 5;
                rookId = isLeftCastle ? 0 : 7;

                CastlingFields.Remove(2);
                CastlingFields.Remove(6);
            }
            else
            {
                bool isLeftCastle = fieldId == 26;
                rookField = isLeftCastle ? 27 : 29;
                rookId = isLeftCastle ? 24 : 31;

                CastlingFields.Remove(26);
                CastlingFields.Remove(30);
            }

            Figure selectedFigure;

            // Move king
            UpdateMatrix(fieldId);
            UpdateReplayAndFigures(fieldId);
            selectedFigure = FigureInfos[_selectedFigureId];
            selectedFigure.NotMoved = false;

            // Move rook
            _selectedFigureId = rookId;
            _selectedFigureField = rookField;

            UpdateMatrix(rookId);
            UpdateReplayAndFigures(rookId);
            selectedFigure = FigureInfos[_selectedFigureId];
            selectedFigure.NotMoved = false;

            _selectedFigureId = -1;
            _selectedFigureField = -1;
        }

        public bool IsEnPassantable(int x, int y)
        {
            return EnPassantField == x + y * 8;
        }

        public void PromoteInto(FigureType type, int figureId = -1)
        {
            _selectedFigureId = figureId == -1 ? _selectedFigureId : figureId; // For replay

            string colorSuffix = PlayerTurn.Equals(FigureColor.White) ? "Light" : "Dark";
            GameObject newFigureObject = _figures.transform.Find(type.ToString() + colorSuffix).gameObject;

            GameObject selectedFigureObject = FigureInfos[_selectedFigureId].gameObject;

            selectedFigureObject.GetComponent<MeshFilter>().mesh =
                newFigureObject.GetComponent<MeshFilter>().mesh;

            selectedFigureObject.GetComponent<MeshRenderer>().materials[0] =
                newFigureObject.GetComponent<MeshRenderer>().materials[0];

            Figure selectedScript = selectedFigureObject.GetComponent<Figure>();
            Figure newScript;

            switch (type)
            {
                case FigureType.Rook:
                    {
                        selectedFigureObject.AddComponent<Rook>();
                        newScript = selectedFigureObject.GetComponent<Rook>();
                        break;
                    }
                case FigureType.Knight:
                    {
                        selectedFigureObject.AddComponent<Knight>();
                        newScript = selectedFigureObject.GetComponent<Knight>();
                        break;
                    }
                case FigureType.Bishop:
                    {
                        selectedFigureObject.AddComponent<Bishop>();
                        newScript = selectedFigureObject.GetComponent<Bishop>();
                        break;
                    }
                case FigureType.Queen:
                    {
                        selectedFigureObject.AddComponent<Queen>();
                        newScript = selectedFigureObject.GetComponent<Queen>();
                        break;
                    }
                case FigureType.Pawn:
                    {
                        selectedFigureObject.AddComponent<Pawn>();
                        newScript = selectedFigureObject.GetComponent<Pawn>();
                        break;
                    }

                default:
                    return;
            }

            newScript.Initialize(selectedScript);
            newScript.AvailableMovesFrom(_selectedFigureField);

            Destroy(selectedScript);
        }


//####################-- PRIVATE HELPER METHODS --######################################################

        bool IsSelectedFigureOfType(FigureType type) 
        {
            return FigureInfos[_selectedFigureId].Type.Equals(type);
        }

        bool IsHorizontalPathToFieldEmptyAndUnexposed(int destField)
        {
            List<int> fields = new();
            int startField = PositionOfFigure(_selectedFigureId);
            int direction = destField > startField ? 1 : -1;
            int field = startField;

            do
            {
                field += direction;
                int x = field % 8;
                int y = field / 8;

                if (BoardMatrix[y, x] != -1) return false;

                fields.Add(field);
            }
            while (field != destField);

            return WillMoveToFieldExposeFields(destField, fields, false);
        }

        void UpdateReplayAndFigures(int fieldId)
        {
            _replayData.StoreMoveBackward(_selectedFigureId, _selectedFigureField);
            _replayData.StoreMoveForward(_selectedFigureId, fieldId);
            _figureMoveChannel.TriggerEvent(_selectedFigureId, fieldId);
        }

        

        bool ResultsInCheck(int fieldId) {
            int kingField = PositionOfFigure(PlayerTurn.Equals(FigureColor.White) ? 4 : 28);
            return WillMoveToFieldExposeFields(fieldId, new List<int>(kingField));
        }

        /* 
        *   Copy current board state
        *   Make a move
        *   Check if exposed
        *   Revert board state
        */
        bool WillMoveToFieldExposeFields(int destination, List<int> exposableFields, bool useDest = true)
        {
            int[,] boardCopy = BoardMatrix;

            if (useDest) 
                UpdateMatrix(destination);

            HashSet<int> enemyReachablePositions = new();
            int offset = PlayerTurn.Equals(FigureColor.Black) ? 0 : 16;

            for (int i = 0 + offset; i < 16 + offset; ++i)
            {
                foreach (int field in FigureInfos[i].AvailableMoves)
                    enemyReachablePositions.Add(field);
            }

            BoardMatrix = boardCopy;

            foreach (int field in exposableFields)
                if (enemyReachablePositions.Contains(field))
                    return true;

            return false;
        }

        int PositionOfFigure(int figureId)
        {
            for (int i = 0; i < 8; ++i)
                for (int j = 0; j < 8; ++j)
                    if (BoardMatrix[j, i] == figureId)
                        return i + j * 8;
            return -1;
        }

        void GetAvailableFieldsFor(int figureId)
        {
            _selectedFigureField = PositionOfFigure(figureId);
            _figureSelectChannel.TriggerEvent(figureId);
        }

        void UpdateMatrix(int fieldId)
        {
            int currentX = _selectedFigureField % 8;
            int currentY = _selectedFigureField / 8;
            int x = fieldId % 8;
            int y = fieldId / 8;

            BoardMatrix[y, x] = _selectedFigureId;
            BoardMatrix[currentY, currentX] = -1;
        }

        void ToggleTurn()
        {
            PlayerTurn = PlayerTurn.Equals(FigureColor.White) ? FigureColor.Black : FigureColor.White;
        }

//####################-- PUBLIC HELPER METHODS --########################################################

        public bool IsFigureOfColorOnField(FigureColor color, int x, int y)
        {
            int figureId = BoardMatrix[y, x];

            if (figureId != -1 && (
                (color.Equals(FigureColor.White) && figureId > -1 && figureId < 16) ||
                (color.Equals(FigureColor.Black) && figureId > 15 && figureId < 32)))
                return true;

            return false;
        }

        public bool IsFieldOccupied(int x, int y)
        {
            return BoardMatrix[y, x] != -1;
        }

        public void ResetMatrix()
        {
            BoardMatrix = _initialBoard;
        }

    }
}
