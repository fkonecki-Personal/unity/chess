using System.Collections;
using UnityEngine;

namespace Chess
{
    public class Replay : MonoBehaviour
    {
        [SerializeField] FigureMoveChannel _figureMoveChannel;
        [SerializeField] ReplayData _replayData;
        [SerializeField] BoardSO _board;

        public static Replay Instance { get; set; }

        int _moveIndex = 0;


//####################-- MONO METHODS --#############################################################

        void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(this);
        }

        void Start()
        {
            _replayData.ClearData();
        }


//####################-- PRIMARY METHODS --#############################################################

        public void Next()
        {
            if (_moveIndex < _replayData.MovesForward.Count - 1)
            {
                ++_moveIndex;

                int figure = _replayData.MovesForward[_moveIndex].Figure;
                int field = _replayData.MovesForward[_moveIndex].Field;

                _figureMoveChannel.TriggerEvent(figure, field);
            }
        }

        public void Previous()
        {
            if (_moveIndex > 0)
            {
                --_moveIndex;

                _figureMoveChannel.TriggerEvent(
                    _replayData.MovesBackward[_moveIndex].Figure,
                    _replayData.MovesBackward[_moveIndex].Field);
            }
        }

//####################-- COROUTINES --#############################################################

        public void StartReplay()
        {
            StartCoroutine(ReplayCoroutine());
        }

        public IEnumerator ReplayCoroutine(int waitTimeSec = 1)
        {
            yield return new WaitForSeconds(waitTimeSec);

            foreach (Move move in _replayData.MovesForward)
            {
                _figureMoveChannel.TriggerEvent(move.Figure, move.Field);
                yield return new WaitForSeconds(waitTimeSec);
            }
        }
    }
}
