using System.Collections.Generic;
using UnityEngine;

namespace Chess {
    [CreateAssetMenu(fileName = "ReplayData", menuName = "Chess/Data/Replay")]
    public class ReplayData : ScriptableObject
    {
        public List<Move> MovesForward { get; set; }
        public List<Move> MovesBackward { get; set; }
        public List<FigureType> PromotionsForward { get; set; }
        public List<FigureType> PromotionsBackward { get; set; }

        public void ClearData() {
            MovesForward = new();
            MovesBackward = new();
            PromotionsForward = new();
            PromotionsBackward = new();
        }

        public void StoreMoveForward(int figure, int field) {
            MovesForward.Add(new Move(figure, field));
        }

        public void StoreMoveBackward(int figure, int field) {
            MovesBackward.Add(new Move(figure, field));
        }

        public void StorePromotionForward(FigureType type)
        {
            PromotionsForward.Add(type);
        }

        public void StorePromotionBackward(FigureType type)
        {
            PromotionsBackward.Add(type);
        }
    }
}
