using System.Collections.Generic;

namespace Chess
{
    public class King : FigureExact
    {
        public override List<int> AvailableMovesFrom(int fieldId)
        {
            List<int> result = new();

            int x = fieldId % 8;
            int y = fieldId / 8;

            int xCheck;
            int yCheck;

            for (int i = -1; i < 2; ++i)
            {
                for (int j = -1; j < 2; ++j)
                {
                    if (i != 0 || j != 0)
                    {
                        xCheck = x + i;
                        yCheck = y + j;
                        if (IsInRange(xCheck) && IsInRange(yCheck) && !Board.IsFigureOfColorOnField(Color, xCheck, yCheck))
                        {
                            result.Add(xCheck + yCheck * 8);
                        }
                    }
                }
            }

            AvailableMoves = result;
            return result;
        }
    }
}