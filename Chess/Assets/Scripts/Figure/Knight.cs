using System.Collections.Generic;

namespace Chess
{
    public class Knight : FigureExact
    {
        public override List<int> AvailableMovesFrom(int fieldId)
        {
            List<int> result = new();

            int x = fieldId % 8;
            int y = fieldId / 8;

            int lJumpX;
            int lJumpY;

            for (int i = -1; i <= 1; i = i + 2)
            {
                for (int j = -2; j <= 2; j = j + 4)
                {
                    lJumpX = x + i;
                    lJumpY = y + j;
                    if (IsInRange(lJumpX) && IsInRange(lJumpY) && !Board.IsFigureOfColorOnField(Color, lJumpX, lJumpY))
                    {
                        result.Add(lJumpX + lJumpY * 8);
                    }

                    lJumpX = x + j;
                    lJumpY = y + i;
                    if (IsInRange(lJumpX) && IsInRange(lJumpY) && !Board.IsFigureOfColorOnField(Color, lJumpX, lJumpY))
                    {
                        result.Add(lJumpX + lJumpY * 8);
                    }
                }
            }
             
            AvailableMoves = result;
            return result;
        }

    }
}