using System.Collections.Generic;

namespace Chess
{
    public class Bishop : FigureDirectional
    {

        public override List<int> AvailableMovesFrom(int fieldId)
        {
            List<int> result = AvailableDiagonalMovesFrom(fieldId);

            AvailableMoves = result;
            return result;
        }

    }
}
