using System.Collections.Generic;

namespace Chess {
    public abstract class FigureDirectional : Figure
    {

        protected List<int> AvailableStraightMovesFrom(int fieldId)
        {
            List<int> result = new();
            FigureColor otherColor = Color.Equals(FigureColor.White) ? FigureColor.Black : FigureColor.White;

            int x = fieldId % 8;
            int y = fieldId / 8;

            int xCheck;
            int yCheck;

            // Up
            yCheck = y + 1;
            while (IsInRange(yCheck) && !Board.IsFigureOfColorOnField(Color, x, yCheck))
            {
                result.Add(yCheck * 8 + x);
                if (Board.IsFigureOfColorOnField(otherColor, x, yCheck))
                    break;

                ++yCheck;
            }

            // Down
            yCheck = y - 1;
            while (IsInRange(yCheck) && !Board.IsFigureOfColorOnField(Color, x, yCheck))
            {
                result.Add(yCheck * 8 + x);
                if (Board.IsFigureOfColorOnField(otherColor, x, yCheck))
                    break;

                --yCheck;
            }

            // Left
            xCheck = x - 1;
            while (IsInRange(xCheck) && !Board.IsFigureOfColorOnField(Color, xCheck, y))
            {
                result.Add(y * 8 + xCheck);
                if (Board.IsFigureOfColorOnField(otherColor, xCheck, y))
                    break;

                --xCheck;
            }

            // Right
            xCheck = x + 1;
            while (IsInRange(xCheck) && !Board.IsFigureOfColorOnField(Color, xCheck, y))
            {
                result.Add(y * 8 + xCheck);
                if (Board.IsFigureOfColorOnField(otherColor, xCheck, y))
                    break;

                ++xCheck;
            }


            return result;
        }

        protected List<int> AvailableDiagonalMovesFrom(int fieldId)
        {
            List<int> result = new();
            FigureColor otherColor = Color.Equals(FigureColor.White) ? FigureColor.Black : FigureColor.White;

            int x = fieldId % 8;
            int y = fieldId / 8;

            int xCheck;
            int yCheck;

            // Up Left
            xCheck = x - 1;
            yCheck = y + 1;
            while (IsInRange(xCheck) && IsInRange(yCheck) && !Board.IsFigureOfColorOnField(Color, xCheck, yCheck))
            {
                result.Add(yCheck * 8 + xCheck);
                if (Board.IsFigureOfColorOnField(otherColor, xCheck, yCheck))
                    break;

                --xCheck;
                ++yCheck;
            }

            // Up Right 
            xCheck = x + 1;
            yCheck = y + 1;
            while (IsInRange(xCheck) && IsInRange(yCheck) && !Board.IsFigureOfColorOnField(Color, xCheck, yCheck))
            {
                result.Add(yCheck * 8 + xCheck);
                if (Board.IsFigureOfColorOnField(otherColor, xCheck, yCheck))
                    break;

                ++xCheck;
                ++yCheck;
            }

            // Down Left
            xCheck = x - 1;
            yCheck = y - 1;
            while (IsInRange(xCheck) && IsInRange(yCheck) && !Board.IsFigureOfColorOnField(Color, xCheck, yCheck))
            {
                result.Add(yCheck * 8 + xCheck);
                if (Board.IsFigureOfColorOnField(otherColor, xCheck, yCheck))
                    break;

                --xCheck;
                --yCheck;
            }

            // Down Right
            xCheck = x + 1;
            yCheck = y - 1;
            while (IsInRange(xCheck) && IsInRange(yCheck) && !Board.IsFigureOfColorOnField(Color, xCheck, yCheck))
            {
                result.Add(yCheck * 8 + xCheck);
                if (Board.IsFigureOfColorOnField(otherColor, xCheck, yCheck))
                    break;

                ++xCheck;
                --yCheck;
            }


            return result;
        }
    }
}
