using System.Collections.Generic;

namespace Chess
{
    public class Rook : FigureDirectional
    {
        public override List<int> AvailableMovesFrom(int fieldId)
        {
            List<int> result = AvailableStraightMovesFrom(fieldId);

            AvailableMoves = result;
            return result;
        }
    }
}
