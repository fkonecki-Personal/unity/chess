using System.Collections.Generic;
using UnityEngine;

namespace Chess
{
    [System.Serializable]
    public abstract class Figure : MonoBehaviour
    {
        public FigureColor Color { get; set; }
        public FigureType Type { get; set; }
        public int Id { get; set; } // 0 - 15 white, 16 - 31 black 
        public bool NotMoved { get; set; } = true;
        public List<int> AvailableMoves { get; set; } = new();
        public Vector3 InitialPosition { get; set; }
        public BoardSO Board { get; set; }

        BoxCollider boxCollider;

        public abstract List<int> AvailableMovesFrom(int fieldId);


//####################-- MONO METHODS --########################################################
        void Awake()
        {
            boxCollider = GetComponent<BoxCollider>();

            int row = (int) transform.position.z;
            int column = (int) transform.position.x;
            Id = (row - (row < 2 ? 0 : 4)) * 8 + column;
            Color = Id < 16 ? FigureColor.White : FigureColor.Black;

            if (Color.Equals(FigureColor.Black))
                boxCollider.enabled = false;

            InitialPosition = transform.position;
        }

        void Start()
        {
            FigureSelectAction(); // To initialize AvailableMoves
            Board.FigureInfos.Insert(Id, this);
        }

//####################-- LISTENER METHODS --########################################################

        public void FigureSelectAction()
        {
            Vector3 position = transform.position;
            int fieldId = (int)position.x + (int)position.z * 8;

            Board.AvailableFields = AvailableMovesFrom(fieldId);
        }

        public void FigureMoveAction(int field)
        {
            // Any move disables en passant
            if (Board.EnPassantField != -1) Board.EnPassantField = -1;

            // If pawn took 2 steps forward, make 1 step forward en passantable
            if (Type.Equals(FigureType.Pawn) && (int)Mathf.Abs(transform.position.z - field / 8) == 2)
                Board.EnPassantField = (field % 8) + (int)(field/16 + transform.position.z/2);

            transform.position = new Vector3(field % 8, transform.position.y, field / 8);
        }


//####################-- HELPER METHODS --########################################################

        public bool IsInRange(int x)
        {
            return x > -1 && x < 8;
        }

        public void ToggleCollider(int figureId) {
            if (figureId < 16)
            {
                boxCollider.enabled = Color.Equals(FigureColor.Black);
            }
            else
            {
                boxCollider.enabled = Color.Equals(FigureColor.White);
            }
        }

        public void ResetPosition() {
            transform.position = InitialPosition;
        }

        public void Initialize(Figure figureScript)
        {
            Color = figureScript.Color;
            Type = figureScript.Type;
            Id = figureScript.Id;
            NotMoved = figureScript.NotMoved;
            InitialPosition = figureScript.InitialPosition;
            Board = figureScript.Board;
        }

    }
}