using System.Collections.Generic;

namespace Chess {
    [System.Serializable]

    public class Pawn : FigureExact
    {
        public bool enPassant { get; set; } = false;

        public override List<int> AvailableMovesFrom(int fieldId)
        {
            FigureColor otherColor = Color.Equals(FigureColor.White) ? FigureColor.Black : FigureColor.White;
            int direction = Color.Equals(FigureColor.White) ? 1 : -1;
            List<int> result = new();

            int x = fieldId % 8;
            int y = fieldId / 8;

            // One step forward
            int stepForward = y + direction;

            if (IsInRange(stepForward))
            {
                if (!Board.IsFieldOccupied(x, stepForward)) {
                    result.Add(stepForward * 8 + x);

                    // Two steps forward
                    if (NotMoved)
                    {
                        int doubleForward = y + 2 * direction;
                        if (IsInRange(doubleForward) && !Board.IsFieldOccupied(x, doubleForward))
                        {
                            result.Add(doubleForward * 8 + x);
                        }
                    }
                }

                // Diagonals
                int xLeft = x - 1;
                if (IsInRange(xLeft) && (
                    (Board.IsEnPassantable(xLeft, stepForward) && Board.IsFigureOfColorOnField(otherColor, xLeft, y)) ||
                    Board.IsFigureOfColorOnField(otherColor, xLeft, stepForward)))

                    result.Add(stepForward * 8 + xLeft);

                int xRight = x + 1;
                if (IsInRange(xRight) && (
                    (Board.IsEnPassantable(xRight, stepForward) && Board.IsFigureOfColorOnField(otherColor, xRight, y)) ||
                    Board.IsFigureOfColorOnField(otherColor, xRight, stepForward)))
                    result.Add(stepForward * 8 + xRight);
               
            }

            AvailableMoves = result;
            return result;
        }
    }
}
