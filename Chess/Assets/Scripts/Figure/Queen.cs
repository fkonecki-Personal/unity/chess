using System.Collections.Generic;
using System.Linq;

namespace Chess
{
    public class Queen : FigureDirectional
    {
        public override List<int> AvailableMovesFrom(int fieldId)
        {
            List<int> result = 
                AvailableStraightMovesFrom(fieldId)
                .Concat(AvailableDiagonalMovesFrom(fieldId))
                .ToList();

            AvailableMoves = result;
            return result;
        }
    }
}