namespace Chess
{
    public class Move
    {
        public int Figure { get; set; }
        public int Field { get; set; }

        public Move(int figure, int field)
        {
            Figure = figure;
            Field = field;
        }
    }
}
