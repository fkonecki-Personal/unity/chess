using UnityEngine;

namespace Chess {
    public class PromoteScript : MonoBehaviour
    {
        [SerializeField] UnityEngine.UI.RawImage _playerImage;
        [SerializeField] GameObject _promotionObject;
        [SerializeField] BoardSO board;

        public int DropdownValue { get; set; }

        public void TogglePlayerTurnColor()
        {
            _playerImage.color = _playerImage.color.Equals(Color.white) ? Color.black : Color.white;
        }

        public void SetPromoteUIActive(bool active)
        {
            _promotionObject.SetActive(active);
        }

        public void Promote(int type)
        {
            board.PromoteInto((FigureType)type);
            SetPromoteUIActive(false);
            TogglePlayerTurnColor();
        }

        public void Promote()
        {
            board.PromoteInto((FigureType)DropdownValue);
        }
    }
}