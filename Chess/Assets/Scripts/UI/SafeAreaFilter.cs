using UnityEngine;

namespace Chess
{
    [RequireComponent(typeof(RectTransform))]
    public class SafeAreaFilter : MonoBehaviour
    {
        private void Awake()
        {
            RectTransform _rectTransform = GetComponent<RectTransform>();
            Rect _safeArea = Screen.safeArea;
            Vector2 _anchorMin = _safeArea.position;
            Vector2 _anchorMax = _anchorMin + _safeArea.size;

            _anchorMin.x /= Screen.width;
            _anchorMin.y /= Screen.height;
            _anchorMax.x /= Screen.width;
            _anchorMax.y /= Screen.height;

            _rectTransform.anchorMin = _anchorMin;
            _rectTransform.anchorMax = _anchorMax;
        }
    }
}
