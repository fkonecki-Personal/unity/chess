using UnityEngine;

namespace Chess
{
    public class Field : MonoBehaviour
    {
        public int Id { get; set; } // 0 - 63

        void Awake()
        {
            Id = (int) (transform.parent.position.z * 8 + transform.position.x);
        }
    }
}
