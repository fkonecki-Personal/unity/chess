using UnityEngine;

namespace Chess {
    public abstract class EventListener : MonoBehaviour
    {
        [SerializeField] protected EventChannel _channel;

        void OnEnable()
        {
            _channel.AddListener(this);
        }

        private void OnApplicationQuit()
        {
            _channel.RemoveListener(this);
        }
    }
}
