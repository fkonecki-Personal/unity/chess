using UnityEngine;
using UnityEngine.Events;

namespace Chess
{
    public class UIListener : EventListener
    {
        [SerializeField] UnityEvent _figureSelectAction;

        int _figureNumber;

        void Start()
        {
            _figureNumber = GetComponent<Figure>().Id;
        }

        public void OnEventTriggered(int id)
        {
            if (_figureNumber == id)
            {
                _figureSelectAction.Invoke();
            }
        }

    }
}
