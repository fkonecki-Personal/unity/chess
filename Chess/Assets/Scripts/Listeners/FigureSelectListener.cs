using UnityEngine.Events;
using UnityEngine;

namespace Chess
{
    [RequireComponent(typeof(Figure))]
    public class FigureSelectListener : EventListener
    {
        [SerializeField] UnityEvent _figureSelectAction;

        int _figureNumber;

        void Start()
        {
            _figureNumber = GetComponent<Figure>().Id;
        }

        public void OnEventTriggered(int id)
        {
            if (_figureNumber == id)
            {
                _figureSelectAction.Invoke();
            }
        }
    }
}
