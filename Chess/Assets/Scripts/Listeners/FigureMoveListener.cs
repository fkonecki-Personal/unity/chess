using UnityEngine;

namespace Chess {

    [RequireComponent(typeof(Figure))]
    public class FigureMoveListener : EventListener
    {
        Figure _figureScript;
        
        void Start()
        {
            _figureScript = GetComponent<Figure>();
        }

        public void OnEventTriggered(int id, int field)
        {
            if (_figureScript.Id == id)
            {
                _figureScript.NotMoved = false;
                _figureScript.FigureMoveAction(field);
            }
            else 
            { 
                int x = (int) transform.position.x;
                int y = (int) transform.position.z;

                if (field == x + y * 8)
                {
                    gameObject.SetActive(!gameObject.activeSelf);
                }
            }

            _figureScript.ToggleCollider(id);
        }

        public void OnEventTriggered()
        {
            gameObject.SetActive(true);
            _figureScript.ResetPosition();
        }
    }
}
