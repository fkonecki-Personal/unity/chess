using UnityEngine;

namespace Chess
{
    public class PlayerInput : MonoBehaviour
    {
        [SerializeField] BoardSO _board;
        [SerializeField] FigureMoveChannel _figureMoveChannel;

        Camera _camera;
        bool _manualReplay = false;

        void Start()
        {
            _camera = Camera.main;
            _board.ResetMatrix();
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                bool hit = Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo);

                if (hit)
                {
                    GameObject selected = hitInfo.transform.gameObject;
                    switch (selected.tag)
                    {
                        case ("Figure"):
                            {
                                int figureId = selected.GetComponent<Figure>().Id;
                                _board.FigureSelected(figureId);
                                Debug.Log("Figure select " + figureId);
                                break;
                            }

                        case ("Field"):
                            {
                                int fieldId = selected.GetComponent<Field>().Id;
                                _board.FieldSelected(fieldId);
                                Debug.Log("Field select " + fieldId);
                                break;
                            }

                        default:
                            break;
                    }
                }
            }
            else if (Input.GetKeyDown(KeyCode.A))
            {
                _figureMoveChannel.TriggerEvent();
                Replay.Instance.StartReplay();
            }
            else if (Input.GetKeyDown(KeyCode.M))
            {
                _figureMoveChannel.TriggerEvent();
                _manualReplay = !_manualReplay;
            }
            else if (_manualReplay)
            {
                if (Input.GetKeyDown(KeyCode.RightArrow))
                    Replay.Instance.Next();
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                    Replay.Instance.Previous();
            }
        }
    }
}
